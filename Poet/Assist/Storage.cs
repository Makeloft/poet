﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace Poet.Assist
{
	internal class Storage : Wrap.IStorage
	{
		public async Task<string[]> LoadLines(string key, Encoding encoding) =>
			await Task.Factory.StartNew(() => File.ReadAllLines(key, encoding ?? Encoding.Default));

		public async Task SaveLines(string key, IEnumerable<string> text, Encoding encoding) =>
			await Task.Factory.StartNew(() => File.WriteAllLines(key, text, encoding ?? Encoding.Default));
		
		public async Task<string> LoadText(string key, Encoding encoding) =>
			await Task.Factory.StartNew(() => File.ReadAllText(key, encoding ?? Encoding.Default));

		public async Task SaveText(string key, string text, Encoding encoding) =>
			await Task.Factory.StartNew(() => File.WriteAllText(key, text, encoding ?? Encoding.Default));

		public async Task<string> SetStorageKey(string suggestedKey, 
			string filter = "Any files (*.*)|*.*")
		{
			var fileOpenPicker = GetFileSavePicker(suggestedKey, filter);
			var result = fileOpenPicker.ShowDialog();
			if (result != true) return null;
			return await Task.FromResult(fileOpenPicker.FileName);
		}

		public async Task<string[]> GetStorageKeys(string filter = "Any files (*.*)|*.*")
		{
			var fileOpenPicker = GetFileOpenPicker(filter);
			var result = fileOpenPicker.ShowDialog();
			if (result != true) return new string[0];
			return await Task.FromResult(fileOpenPicker.FileNames);
		}

		private static OpenFileDialog GetFileOpenPicker(string filter) =>
			new OpenFileDialog
			{
				Multiselect = true,
				Filter = filter 
			};

		private static SaveFileDialog GetFileSavePicker(string suggestedKey, string filter) =>
			new SaveFileDialog {Filter = filter, FileName = suggestedKey};

		public async Task<bool> Exists(string key) => await Task.Run(() => File.Exists(key));
		
		public async Task Delete(string key) => await Task.Run(() => File.Delete(key));
	}
}