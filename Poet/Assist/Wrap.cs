﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;

namespace Poet.Assist
{
	public class Wrap
	{
		public static IMessanger Messanger { get; set; }

		public interface IMessanger
		{
			bool? Ask(string question, string title = "");
			void Alert(string notification, string title = "");
			void ConfirmExit(string question, string title = "", CancelEventArgs args = null);
		}

		public static IStorage Storage { get; set; }

		public interface IStorage
		{
			Task<string[]> LoadLines(string key, Encoding encoding = null);
			Task SaveLines(string key, IEnumerable<string> text, Encoding encoding = null);
			Task<string> LoadText(string key, Encoding encoding = null);
			Task SaveText(string key, string text, Encoding encoding = null);
			Task<string> SetStorageKey(string suggestedKey, string filter = "Any files (*.*)|*.*");
			Task<string[]> GetStorageKeys(string filter = "Any files (*.*)|*.*");
			Task<bool> Exists(string key);
			Task Delete(string key);
		}
	}
}
