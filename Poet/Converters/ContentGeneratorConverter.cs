using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Ace;

namespace Poet.Converters
{
	public class ContentGeneratorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
			new ContentControl().To(out var c).With
			(
				c.ContentTemplate = (DataTemplate)parameter,
				c.SetBinding(ContentControl.ContentProperty, new Binding())
			);

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
			throw new NotImplementedException();
	}
}