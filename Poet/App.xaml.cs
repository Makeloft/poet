﻿using System.Windows;
using System.Windows.Media;
using Ace;
using Ace.Specific;
using Poet.Assist;
using Poet.ViewModels;

namespace Poet
{
	public partial class App
	{
		private void App_OnStartup(object sender, StartupEventArgs e)
		{
			var box = new Memory(new KeyFileStorage());
			box.DecodeFailed += (key, type, exception) => MessageBox.Show(exception.Message + exception.InnerException);
			box.EncodeFailed += (key, item, exception) => MessageBox.Show(exception.Message + exception.InnerException);
			Store.ActiveBox = box;
			
			Store.Get<AppViewModel>();

			Resources["SmartStateConverter"].To();
			
			Wrap.Messanger = new Messenger();
			Wrap.Storage = new Storage();
		}

		private void App_OnExit(object sender, ExitEventArgs e) => Store.Snapshot();
	}
}