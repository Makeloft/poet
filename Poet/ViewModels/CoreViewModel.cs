﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Ace;
using Poet.Assist;
using Poet.Models;
using Poet.ViewModels.Documnets;
using Poet.Views;

namespace Poet.ViewModels
{
	[DataContract]
	public class CoreViewModel : SmartViewModel
	{
		[DataMember]
		public SmartSet<ADocument> Documents { get; private set; }

		[DataMember]
		public ADocument ActiveDocument
		{
			get => Get(() => ActiveDocument);
			set => Set(() => ActiveDocument, value);
		}

		[DataMember] public FractalPresenter ChildPresenter { get; set; }

		public override void Expose()
		{
			base.Expose();

			Documents = Documents.OrNew();
			Documents.ForEach(d => d.Expose()).ForEach(async d => await d.TryLoadAsync());
			Documents.CollectionChangeCompleted += (sender, args) =>
				args.NewItems?.Cast<ADocument>().LastOrDefault().To(out var document)?.With(ActiveDocument = document);

			this[() => ActiveDocument].PropertyChanged += (sender, args) =>
				Context.Get("Activate").EvokeCanExecuteChanged();

			this[Context.Exit].Executed += (sender, args) =>
			{
				Store.Snapshot();
				Documents.OfType<PlainTextDocument>().ForEach(async d => await d.SaveBackup());
				var question = LocalizationSource.Wrap["DoYouWantToExitQuestion"];
				Wrap.Messanger.ConfirmExit(question, this["Title"] as string, args.Parameter as CancelEventArgs);
			};

			this[ApplicationCommands.New].Executed += (sender, args) => Add(false, args.Parameter);
			this[ApplicationCommands.Open].Executed += (sender, args) => Add(true, args.Parameter);

			this[ApplicationCommands.Close].Executed += (sender, args) => Close(args.Parameter ?? Documents);
			this[ApplicationCommands.Close].CanExecute += (sender, args) =>
				args.CanExecute = args.Parameter.Is<ADocument>() || Documents.Any();

			this[ApplicationCommands.SaveAs].CanExecute += (sender, args) => args.CanExecute = Documents.Any();
			this[ApplicationCommands.SaveAs].Executed += async (sender, args) => await ActiveDocument.TrySaveAsAsync();
			this[ApplicationCommands.Save].Executed += (sender, args) => Save(args.Parameter ?? Documents);
			this[ApplicationCommands.Save].CanExecute += (sender, args) =>
				args.CanExecute = ActiveDocument.Is() && ActiveDocument.Is(args.Parameter)
					? ActiveDocument.HasChanged
					: Documents.Any(d => d.HasChanged);

			this[Context.Get("About")].Executed += (sender, args) =>
				new AboutView { Owner = Application.Current.MainWindow }.ShowDialog();
		}

		private async void Add(bool open, object parameter) =>
			(await CreateDocuments(Store.Get<FactoryViewModel>(), open, parameter)).ForEach(d => d.Expose())
			.ForEach(Documents.Add).ForEach(async d => await d.TryLoadAsync());

		private static async Task<IList<ADocument>> CreateDocuments(FactoryViewModel factory,
			bool open, object parameter) =>
			open ? await factory.Open(parameter) : await factory.Create(parameter);

		private static IEnumerable<ADocument> GetDocuments(object parameter) =>
			parameter.Is(out ADocument document) ? document.ToEnumerable() : (IList<ADocument>)parameter;

		private static void Save(object parameter) =>
			GetDocuments(parameter).Where(d => d.HasChanged).ForEach(async d => await d.TrySaveAsync());

		private void Close(object parameter) =>
			GetDocuments(parameter).ToArray().ForEach(async d => await d.TryCloseAsync() && Documents.Remove(d))
				.OfType<PlainTextDocument>().ForEach(async d => await d.DeleteBackupAsync());
	}
}