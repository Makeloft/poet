﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ace;
using Poet.Assist;
using Poet.ViewModels.Documnets;

namespace Poet.ViewModels
{
	internal class FactoryViewModel : ContextObject
	{
		public async Task<IList<ADocument>> Create(object parameter) => new ADocument[]
		{
			new PlainTextDocument()
			//Console.CapsLock.Not() ? (ADocument)new WebDocument() : new MultiLineDocument()
		};

		private static string DefaultOpenFilter =
			"Supported Files (*.txt;*.log;*.cvs;*.mdf)|*.txt;*.log;*.cvs;*.mdf|" +
			"Any files (*.*)|*.*";
		
		public async Task<IList<ADocument>> Open(object parameter) =>
			(await Wrap.Storage.GetStorageKeys(DefaultOpenFilter)).Select(Create).ToArray();
		
		public ADocument Create(string key) => new MultiLineDocument {Key = key};
	}
}