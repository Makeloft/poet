﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Controls;
using Ace;
using Poet.Behaviours;

namespace Poet.ViewModels.Documnets
{
	[DataContract]
	class WebDocument : ADocument<string, WebBrowser>
	{
		static WebDocument()
		{
			var AppPath = App.Current.GetType().Assembly.Location;
			Assist.BrowserEmulation.EnableFor(AppPath);
#if DEBUG
			Assist.BrowserEmulation.EnableFor(AppPath + ".vshost");
#endif
		}

		public override void Expose()
		{
			base.Expose();
			Title = "New page";

			this[() => Key].PropertyChanged += (o, e) => Context.Get("Go").EvokeCanExecuteChanged();
			this[Context.Get("Go")].CanExecute += (o, e) =>	e.CanExecute = Key.IsNullOrWhiteSpace().Not();
			this[Context.Get("Go")].Executed += async (o, e) =>
			{
				try
				{
					IsBusy = true;
					var key = Key;
					key = key.EndsWith(":") ? key + "\\" : key;
					if (Directory.Exists(key) || key.StartsWith("http://") || key.StartsWith("https://"))
					{
						Value.Navigate(key);
					}
					else
					{
						try
						{
							var addresses = await Task.Factory.StartNew(() =>
								Dns.GetHostAddresses($"http://{key}".ToUri().Host));

							Value.Navigate($"http://{key}");
						}
						catch
						{
							Value.Navigate($"https://duckduckgo.com/?q={key}");
						}	
					}
				}
				catch (Exception exeption)
				{
					Trace.WriteLine(exeption);
				}
				finally
				{
					IsBusy = false;
				}
			};

			this[Context.Next].CanExecute += (o, e) => e.CanExecute = Value.CanGoForward;
			this[Context.Next].Executed += (o, e) => Value.GoForward();

			this[Context.Back].CanExecute += (o, e) => e.CanExecute = Value.CanGoBack;
			this[Context.Back].Executed += (o, e) => Value.GoBack();
		}

		public void Bind(WebBrowser browser)
		{
			Value = browser;

			browser.Navigating += (o, e) => IsBusy = true;
			browser.Navigated += (o, e) => IsBusy = false;
			browser.Navigated += (o, e) =>
			{	
				var uri = e.Uri.ToStr();
				var key = uri.StartsWith("file:///") ? new DirectoryInfo(uri.Replace("file:///", "")).FullName : uri;
				if (Key.IsNot(key)) Key = key;
				Title = browser.Document.Is(out mshtml.IHTMLDocument2 d)
					? d.title.IsNullOrWhiteSpace() ? Title : d.title
					: uri.StartsWith("file:///") ? new DirectoryInfo(uri.Replace("file:///", "")).Name : uri; ;

				Context.Back.EvokeCanExecuteChanged();
				Context.Next.EvokeCanExecuteChanged();

				var nativeService = browser.GetNativeService();
				if (nativeService.Is())
				{
					nativeService.Silent = true;
					nativeService.NewWindow3 -= NativeService_NewWindow3;
					nativeService.NewWindow3 += NativeService_NewWindow3;
				}

				browser.Focus();
			};

			var mediator = this.GetMediator(Context.Get("Go"));
			if (mediator.CanExecute(null)) mediator.Execute(null);
		}

		private void NativeService_NewWindow3(ref object ppDisp, ref bool Cancel, uint dwFlags, string bstrUrlContext, string bstrUrl)
		{
			var newPage = new WebDocument { Key = bstrUrl };
			newPage.Expose();
			Store.Get<CoreViewModel>().Documents.Add(newPage);
			Cancel = true;
		}

		public override Task<bool> TryCloseAsync() => Task.FromResult(true);
		public override Task<bool> TryLoadAsync() => Task.FromResult(true);
		public override Task<bool> TrySaveAsAsync() => Task.FromResult(true);
		public override Task<bool> TrySaveAsync() => Task.FromResult(true);
	}
}
