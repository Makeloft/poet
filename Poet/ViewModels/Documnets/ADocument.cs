﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Ace;
using Poet.Assist;
using Poet.Models;

namespace Poet.ViewModels.Documnets
{
	[DataContract]
	public abstract class ADocument<TKey, TValue> : ADocument
	{
		[DataMember]
		public TKey Key
		{
			get => Get(() => Key);
			set => Set(() => Key, value);
		}

		public TValue Value
		{
			get => Get(() => Value);
			set => Set(() => Value, value, true);
		}
	}
	
	[DataContract]
	public abstract class ADocument : ContextObject, IExposable
	{	
		[DataMember]
		public FractalPresenter ChildPresenter { get; set; }
		
		[DataMember]
		public virtual string Title
		{
			get => Get(() => Title);
			set => Set(() => Title, value);
		}

		[DataMember]
		public virtual bool HasChanged
		{
			get => Get(() => HasChanged);
			set => Set(() => HasChanged, value);
		}

		[DataMember]
		public virtual bool IsBusy
		{
			get => Get(() => IsBusy);
			set => Set(() => IsBusy, value);
		}

		public abstract Task<bool> TryLoadAsync();

		public abstract Task<bool> TrySaveAsync();

		public abstract Task<bool> TrySaveAsAsync();
		
		public abstract Task<bool> TryCloseAsync();

		public virtual void Expose()
		{
			ChildPresenter.OrNew().To(out var p).With(p.Content = this, ChildPresenter = p);
			var core = Store.Get<CoreViewModel>();
			this[Context.Activate].CanExecute += (sender, args) => args.CanExecute = core.ActiveDocument.IsNot(this);
			this[Context.Activate].Executed += (sender, args) => core.ActiveDocument = this;
			
#if DEBUG
			SmartPropertyChanged += (sender, args) =>
				Debug.WriteLine($"{Title}: {args.PropertyName}\t{this[args.PropertyName]}");
#endif
		}

		public override string ToString() => Title;

		protected async Task<T> Handle<T>(Func<Task<T>> func)
		{
			try
			{
				IsBusy = true;
				return await func();
			}
			catch (Exception exception)
			{
				Wrap.Messanger.Alert(exception.ToString());
				return default;
			}
			finally
			{
				IsBusy = false;
				System.Windows.Input.CommandManager.InvalidateRequerySuggested();
			}
		}
	}
}