﻿using System.Collections.Generic;

namespace Poet.Languages
{
	public class English : ALanguage<English>
	{
		public override Dictionary<string, string> GetDictionary() => new Dictionary<string, string>
		{
			{"AppTitle", "Poet"},
			
			{"App", "_App"},
			{"True", "_True"},
			{"False", "_False"},
			
			{"Yes", "_Yes"},
			{"No", "_No"},
			{"Ok", "_Ok"},
			{"Cancel", "_Cancel"},
			{"Apply", "_Apply"},
			{"Discard", "_Discard"},
			{"Retry", "_Retry"},
			
			{"Document", "_Document"},
			{"Documents", "Document_s"},
			{"Character", "Character"},
			{"Word", "Word"},
			{"Line", "Line"},
			{"Paragraph", "Paragraph"},
			{"Page", "Page"},
			
			{"File", "_File"},
			{"Edit", "_Edit"},
			{"Content", "_Content"},
			{"Format", "_Format"},
			{"View", "_View"},
			{"Help", "_Help"},
			
			{"Encoding", "_Encoding"},

			{"About", "_About"},

			{"New", "_New"},
			{"Open", "_Open"},
			{"Save", "_Save"},
			{"SaveAs", "Save _As"},
			{"SaveAll", "Save _All"},
			{"CloseAll", "Close _All"},
			{"Close", "_Close"},
			{"Exit", "_Exit"},

			{"Undo", "_Undo"},
			{"Redo", "_Redo"},

			{"Cut", "Cu_t"},
			{"Copy", "_Copy"},
			{"Paste", "_Paste"},
			{"Delete", "De_lete"},

			{"By", "_By"},
			{"To", "_To"},
			{"As", "_As"},
			{"Left", "_Left"},
			{"Right", "_Right"},
			{"Down", "_Down"},
			{"Up", "_Up"},
			{"Select", "_Select"},
			{"SelectAll", "Select _All"},
			{"Commands", "_Commands"},
			{"Elements", "_Elements"},
			{"Directions", "_Directions"},
			
			{"Topmost", "Topmost"},
			{"TaskBar", "Task Bar"},
			{"ToolBar", "Tool Bar"},
			{"ToolBarTray", "Tool Bar Tray"},
			{"StatusBar", "Status Bar"},
			{"Text", "_Text"},
			{"Font", "_Font"},
			{"Size", "_Size"},
			{"Windows", "_Windows"},
			{"Language", "_Language"},
			{"Family", "Famil_y"},
			{"SystemTray", "System Tray"},
			{"Formatting", "Formatting"},
			{"Rendering", "Rendering"},
			{"Hinting", "Hinting"},
			{"Window", "_Window"},
			{"ShowIn", "Show _In"},
			{"State", "St_ate"},
			{"Style", "St_yle"},
			{"ResizeMode", "Re_size Mode"},
			{"Resize", "Re_size"},
			
			{"Show", "Sho_w"},
			{"Hide", "_Hide"},
			{"Normalize", "Norma_lize"},
			{"Minimize", "Mi_nimize"},
			{"Maximize", "Ma_ximize"},
			{"Menu", "Men_u"},
			{"Alignment", "Ali_gnment"},
			
			{"Print", "_Print"},
			{"Reload", "_Reload"},
			{"Load", "_Load"},
			{"Folder", "_Folder"},
			{"Explorer", "_Explorer"},
			{"Cmd", "C_md"},
			
			{"Name", "_Name"},
			{"Description", "_Description"},
			{"CodePage", "C_ode Page"},
			
			{"Change", "_Change"},
			{"Detect", "_Detect"},
			{"Hot", "_Hot"},
			{"System", "_System"},
			
			{"Start", "_Start"},
			{"Length", "_Length"},
			{"Offset", "_Offset"},
		};
	}
}