﻿using System.Collections.Generic;
using System.Globalization;
using System.Resources;

namespace Poet.Languages
{
	public class Russian : ALanguage<Russian>
	{
		public override Dictionary<string, string> GetDictionary() => new Dictionary<string, string>
		{
			{"File", "Файл"},
			{"Edit", "Правка"},
			{"View", "Вид"},
			{"Help", "Помощь"},
		};
	}
}