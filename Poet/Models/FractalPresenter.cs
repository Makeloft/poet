﻿using Ace;

namespace Poet.Models
{
	[DataContract]
	public class FractalPresenter : ContextObject, IExposable
	{
		[DataMember]
		public object Content { get; set; }

		[DataMember]
		public FractalPresenter ChildPresenter
		{
			get => Get(() => ChildPresenter);
			set => Set(() => ChildPresenter, value);
		}

		[DataMember]
		public bool IsExpanded
		{
			get => Get(() => IsExpanded);
			set => Set(() => IsExpanded, value);
		}

		public string SplitStateView
		{
			get => IsExpanded ? SplitState : "* ^ 0";
			set => SplitState = IsExpanded ? value : SplitState;
		}

		[DataMember] public string SplitState { get; set; } = "* ^ *";

		public FractalPresenter() => Expose();

		public void Expose()
		{
			this[Context.Get("Invert")].Executed += (sender, args) => IsExpanded = IsExpanded.Not();
			this[() => IsExpanded].PropertyChanged += (sender, args) => EvokePropertyChanged(() => SplitStateView);
			this[() => IsExpanded].PropertyChanged += (sender, args) =>
			{
				if (IsExpanded.Is(false)) return;
				ChildPresenter.OrNew().To(out var c).With
				(
					c.Content = Content,
					ChildPresenter = c
				);
			};
		}
	}
}