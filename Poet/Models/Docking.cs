﻿using System.Linq;
using Ace;

namespace Poet.Models
{
	public enum Orientation { Vertical, Horizontal }

	public interface IPanel
	{

	}

	[DataContract]
	public class DoublePanel : ContextObject, IExposable, IPanel
	{
		public IPanel FirstPanel { get; set; }
		public IPanel SecondPanel { get; set; }

		public Orientation Orientation { get; set; }

		[DataMember]
		public bool IsExpanded
		{
			get => Get(() => IsExpanded);
			set => Set(() => IsExpanded, value);
		}

		[DataMember] public string SplitState { get; set; } = "* ^ *";

		public string SplitStateView
		{
			get => IsExpanded ? SplitState : "* ^ 0";
			set => SplitState = IsExpanded ? value : SplitState;
		}

		public DoublePanel() => Expose();

		public void Expose()
		{
			this[Context.Get("Invert")].Executed += (sender, args) => IsExpanded = IsExpanded.Not();
			this[() => IsExpanded].PropertyChanged += (sender, args) => EvokePropertyChanged(() => SplitStateView);
			this[() => IsExpanded].PropertyChanged += (sender, args) =>
			{
				if (IsExpanded.Is(false)) return;

			};
		}
	}

	[DataContract]
	public class SinglePanel : ContextObject, IExposable, IPanel
	{
		public IPanel Panel { get; set; }

		public Orientation Orientation { get; set; }

		[DataMember]
		public bool IsExpanded
		{
			get => Get(() => IsExpanded);
			set => Set(() => IsExpanded, value);
		}

		[DataMember] public string SplitState { get; set; } = "* ^ *";

		public string SplitStateView
		{
			get => IsExpanded ? SplitState : "* ^ 0";
			set => SplitState = IsExpanded ? value : SplitState;
		}

		public SinglePanel() => Expose();

		public void Expose()
		{
			this[Context.Get("Invert")].Executed += (sender, args) => IsExpanded = IsExpanded.Not();
			this[() => IsExpanded].PropertyChanged += (sender, args) => EvokePropertyChanged(() => SplitStateView);
			this[() => IsExpanded].PropertyChanged += (sender, args) =>
			{
				if (IsExpanded.Is(false)) return;

			};
		}
	}

	[DataContract]
	public class TabPanel : ContextObject, IExposable, IPanel
	{
		[DataMember]
		public SmartSet<object> Items { get; set; }

		[DataMember]
		public object ActiveItem
		{
			get => Get(() => ActiveItem) ?? Items.FirstOrDefault();
			set => Set(() => ActiveItem, value);
		}

		public TabPanel() => Expose();

		public void Expose()
		{

		}
	}
}